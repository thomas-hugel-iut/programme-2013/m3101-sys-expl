Copyright Thomas Hugel 2016-2021.

This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.

In any reuse of this work, please include a link to: https://thomas.hugel.tf/

To view a copy of this license, visit https://creativecommons.org/licenses/by-nc-sa/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
