import java.util.Map;

public abstract class Cache {
		
	protected final int capacity;
	protected Map<String, String> map;
	
	Cache (int capacity) {
		this.capacity = capacity;
		reset();
	}
	
	public abstract void reset();
	
	public abstract String get (String key);
	
	/**
	 * If key was already present, does not change the ordering, and returns the previous value.
	 * Otherwise, returns null.
	 */
	public abstract String put (String key, String value);
	
	public abstract void printType();

	protected void print(boolean hit) {
		System.out.print("[ ");
		for (String key : map.keySet())
			System.out.print(key + " ");
		System.out.println("] " + (hit ? "*" : ""));
	}
}

// © Thomas Hugel 2016.
// License: Creative Commons BY-NC-SA 4.0.
