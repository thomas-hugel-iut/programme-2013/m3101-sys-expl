public class Processor implements Runnable {
	private int nbTasks;
	private Scheduler scheduler;
	Thread thread;
	
	public Processor (Scheduler scheduler, int nbTasks) {
		this.scheduler = scheduler;
		this.nbTasks = nbTasks;
		thread = new Thread(this);
		thread.start();
	}

	@Override
	public void run() {
		while (nbTasks > 0) {
			if (scheduler.processNextTask())
				--nbTasks;
		}
	}			
}

// © Thomas Hugel 2016.
// License: Creative Commons BY-NC-SA 4.0.
