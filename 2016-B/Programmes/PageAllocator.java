class PageAllocator {
	public PageAllocator(int capacity) {}
	
	// an exception is thrown when the initial capacity is exceeded
	public int allocate() throws NoSuchElementException {}
	
	public void free(int i) {}
	
	public void printFreePages() {}
}

// © Thomas Hugel 2016.
// License: Creative Commons BY-NC-SA 4.0.
