public abstract class Scheduler {
	public void register (Task task) {}
	
	// @return whether next task is over
	public abstract boolean processNextTask();
	
	protected abstract void put(Task task);
	
	protected abstract Task takeNextTask();

	protected void simulateProcessor (int duration) {
		try {
			Thread.sleep(duration);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}

// © Thomas Hugel 2016.
// License: Creative Commons BY-NC-SA 4.0.
