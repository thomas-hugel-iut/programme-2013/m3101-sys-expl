public class Task {
	final public int id, duration, deadline;
	
	public Task (int id, int duration, int deadline) {
		this.id = id;
		this.duration = duration;
		this.deadline = deadline;
	}
	
	public void print () {
		System.out.print("Task: " + id + "; duration: " + duration + "; deadline: " + deadline);
	}
	
	public void println () {
		print();
		System.out.println();
	}
}

// © Thomas Hugel 2016.
// License: Creative Commons BY-NC-SA 4.0.
