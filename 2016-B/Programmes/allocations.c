#define _BSD_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>

int main(int argc, const char * argv[]) {
    int *p, size = 100000000;
    static struct timeval t1, t2, t;

    gettimeofday(&t1, NULL);
	int i;
    for (i = 0; i < size; ++i) {
        p = malloc(sizeof(int));
        free(p);
    }
    gettimeofday(&t2, NULL);
    timersub(&t2, &t1, &t);
    printf("Time elapsed: %ld.%06ld\n", (long)t.tv_sec, (long)t.tv_usec);

    gettimeofday(&t1, NULL);
    p = malloc(size * sizeof(int));
    free(p);
    gettimeofday(&t2, NULL);
    timersub(&t2, &t1, &t);
    printf("Time elapsed: %ld.%06ld\n", (long)t.tv_sec, (long)t.tv_usec);
   
    return 0;
}

// © Thomas Hugel 2016.
// License: Creative Commons BY-NC-SA 4.0.
